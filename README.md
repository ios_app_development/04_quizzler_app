# 04_Quizzler_app

## Challenge #1.

## This program a trivia quiz app, inspired by the awesome “Quiz Up” game.

* Shows your score based on correct answers
* Show's progress bar and count of answered questions
* Can start over

## Visual Presentation

### Preview
![Static image](documentation/screen_1.png)
![Gif example](https://media.giphy.com/media/5tbePHmi6ev7m0XD2J/giphy.gif)


